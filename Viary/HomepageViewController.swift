//
//  HomepageViewController.swift
//  Viary
//
//  Created by Andre Lay, Gregorius Albert, and Kevin Putra Yonathan on 03/12/21.
//

import UIKit

class HomepageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var arrDiaries = [Diary]()
    var diary:Diary?
    
    @IBOutlet weak var tableView: UITableView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        reload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reload()
    }
    
    func reload() -> Void {
        let context = appDelegate.persistentContainer.viewContext
        let username = UserDefaults.standard.string(forKey: "USERNAME")
        do{
            let fetchDiaries = Diary.showUserDiaries(username: username!)
            let diariesResult = try context.fetch(fetchDiaries)
            arrDiaries = diariesResult
        } catch {
            print(error.localizedDescription)
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDiaries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = arrDiaries[indexPath.row].title
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, YYYY"
        
        cell?.detailTextLabel?.text = dateFormatter.string(from: arrDiaries[indexPath.row].date!)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        diary = arrDiaries[indexPath.row]
        performSegue(withIdentifier: "homepageToEditSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let context = appDelegate.persistentContainer.viewContext
            
            do{
                let diary = arrDiaries[indexPath.row]
                context.delete(diary)
                try context.save()
                
                arrDiaries.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "homepageToEditSegue" {
            let destination = segue.destination as! EditDiaryViewController
            destination.diary = diary
        }
    }
    
    @IBAction func unwindToHomepage(_ unwindSegue: UIStoryboardSegue) {
        //        let sourceViewController = unwindSegue.source
    }
    
    
    
}
