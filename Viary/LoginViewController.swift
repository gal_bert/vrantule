//
//  ViewController.swift
//  Viary
//
//  Created by Andre Lay, Gregorius Albert, and Kevin Putra Yonathan on 03/12/21.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func login(_ sender: Any) {
        let username = usernameTextfield.text!.trimmingCharacters(in: .whitespacesAndNewlines).lowercased()
        let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let context = appDelegate.persistentContainer.viewContext
        
        
        do{
            let fetchRequest = User.checkUserExist(username: username)
            let result = try context.fetch(fetchRequest)
            
            if result.isEmpty {
                present(Helper.pushAlert(title: "Oops!", message: "Credentials not found!"), animated: true, completion: nil)
            }
            else {
                if result[0].password != password {
                    present(Helper.pushAlert(title: "Oops!", message: "These credentials don't match!"), animated: true, completion: nil)
                }
                else {
                    UserDefaults.standard.set(username, forKey: "USERNAME")
                    UserDefaults.standard.set(result[0].fullname, forKey: "FULLNAME")
                    performSegue(withIdentifier: "loginToHomepage", sender: self)
                }
            }
        }
        catch {
            print(error.localizedDescription)
        }
        
    }
    
    @IBAction func unwindToLogin(_ unwindSegue: UIStoryboardSegue) {
//        let sourceViewController = unwindSegue.source
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func usernameDidEndOnExit(_ sender: Any) {
    
    }
    @IBAction func passwordDidEndOnExit(_ sender: Any) {
    
    }
}

