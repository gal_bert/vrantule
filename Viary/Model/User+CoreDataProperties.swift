//
//  User+CoreDataProperties.swift
//  Viary
//
//  Created by Gregorius Albert on 07/01/22.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }
    
    @nonobjc public class func checkUserExist(username: String) -> NSFetchRequest<User> {
        let fetchRequest = NSFetchRequest<User>(entityName: "User")
        let predicate = NSPredicate(format: "username == %@", "\(username)")
        fetchRequest.predicate = predicate
        return fetchRequest
    }

    @NSManaged public var fullname: String?
    @NSManaged public var username: String?
    @NSManaged public var password: String?
    @NSManaged public var diaries: NSSet?

}

// MARK: Generated accessors for diaries
extension User {

    @objc(addDiariesObject:)
    @NSManaged public func addToDiaries(_ value: Diary)

    @objc(removeDiariesObject:)
    @NSManaged public func removeFromDiaries(_ value: Diary)

    @objc(addDiaries:)
    @NSManaged public func addToDiaries(_ values: NSSet)

    @objc(removeDiaries:)
    @NSManaged public func removeFromDiaries(_ values: NSSet)

}

extension User : Identifiable {

}
