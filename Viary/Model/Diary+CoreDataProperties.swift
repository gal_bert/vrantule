//
//  Diary+CoreDataProperties.swift
//  Viary
//
//  Created by Gregorius Albert on 07/01/22.
//
//

import Foundation
import CoreData


extension Diary {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Diary> {
        return NSFetchRequest<Diary>(entityName: "Diary")
    }
    
    @nonobjc public class func showUserDiaries(username: String) -> NSFetchRequest<Diary> {
        let fetchRequest = NSFetchRequest<Diary>(entityName: "Diary")
        let predicate = NSPredicate(format: "user.username == %@", "\(username)")
        fetchRequest.predicate = predicate
        return fetchRequest
    }

    @NSManaged public var date: Date?
    @NSManaged public var title: String?
    @NSManaged public var content: String?
    @NSManaged public var user: User?

}

extension Diary : Identifiable {

}
