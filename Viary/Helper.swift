//
//  Helper.swift
//  Viary
//
//  Created by Andre Lay, Gregorius Albert, and Kevin Putra Yonathan on 03/12/21.
//

import Foundation
import UIKit

class Helper{
    
    static func pushAlert(title:String, message:String) -> UIAlertController{
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: "OK",
            style: .default,
            handler: nil
        ))
        
        return alert
    }
    
    static func isAllCharacters(text:String) -> Bool {
        var allChars = true
        for i in text{
            if i.isNumber{
                allChars = false
                break
            }
        }
        return allChars
    }
    
    static func initializeTextview(textView:UITextView) -> Void {
        let borderColor:UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = borderColor.cgColor
        textView.layer.cornerRadius = 5.0
    }
    
}

