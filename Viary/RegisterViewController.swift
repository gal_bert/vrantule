//
//  RegisterViewController.swift
//  Viary
//
//  Created by Andre Lay, Gregorius Albert, and Kevin Putra Yonathan on 03/12/21.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var fullnameTextfield: UITextField!
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var confirmPasswordTextfield: UITextField!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func register(_ sender: Any) {
        let fullname = fullnameTextfield.text!.trimmingCharacters(in: .whitespacesAndNewlines).capitalized
        let username = usernameTextfield.text!.trimmingCharacters(in: .whitespacesAndNewlines).lowercased()
        let password = passwordTextfield.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let confirmPassword = confirmPasswordTextfield.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if fullname == "" || username == "" || password == "" || confirmPassword == "" {
            present(Helper.pushAlert(title: "Oops!", message: "All fields must be filled!"), animated: true, completion: nil)
        }
        else {
            if !Helper.isAllCharacters(text: fullname){
                present(Helper.pushAlert(title: "Oops!", message: "Fullname cannot contains numbers!"), animated: true, completion: nil)
            }
            else if username.count < 5 {
                present(Helper.pushAlert(title: "Oops!", message: "Username must be minimum 5 characters!"), animated: true, completion: nil)
            }
            else if password != confirmPassword {
                present(Helper.pushAlert(title: "Oops!", message: "Password confirmation do not match!"), animated: true, completion: nil)
            }
            else {
                let context = appDelegate.persistentContainer.viewContext
                
                do{
                    let fetchRequest = User.checkUserExist(username: username)
                    let result = try context.fetch(fetchRequest)
                    
                    if !result.isEmpty {
                        present(Helper.pushAlert(title: "Oops!", message: "Username has been taken!"), animated: true, completion: nil)
                    }
                    
                    else {
                        let user = User(context: context)
                        user.fullname = fullname
                        user.password = password
                        user.username = username
                        
                        context.insert(user)
                        try context.save()
                        
                        let alert = UIAlertController(
                            title: "Register Success!",
                            message: "Please login with your credentials!",
                            preferredStyle: .alert
                        )
                        
                        alert.addAction(UIAlertAction(
                            title: "OK",
                            style: .default,
                            handler: {
                                (alert: UIAlertAction!) in
                                self.performSegue(withIdentifier: "registerToLoginSegue", sender: self)
                            }
                        ))
                        
                        present(alert, animated: true, completion: nil)
                    }
                    
                }
                catch let error {
                    print(error.localizedDescription)
                }
                
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func fullnameDidEndOnExit(_ sender: Any) {
        
    }
    
    @IBAction func usernameDidEndOnExit(_ sender: Any) {
        
    }
    
    
    @IBAction func passwordDidEndOnExit(_ sender: Any) {
        
    }
    
    
    @IBAction func confirmPasswordDidEndOnExit(_ sender: Any) {
        
    }
    
}
