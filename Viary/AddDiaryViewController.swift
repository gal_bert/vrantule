//
//  AddDiaryViewController.swift
//  Viary
//
//  Created by Andre Lay, Gregorius Albert, and Kevin Putra Yonathan on 03/12/21.
//

import UIKit

class AddDiaryViewController: UIViewController {
    @IBOutlet weak var titleTextfield: UITextField!
    @IBOutlet weak var contentTextview: UITextView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.initializeTextview(textView: contentTextview)
    }
    
    
    @IBAction func create(_ sender: Any) {
        let title = titleTextfield.text!.trimmingCharacters(in: .whitespacesAndNewlines).capitalized
        let content = contentTextview.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if title == "" || content == "" {
            present(Helper.pushAlert(title: "Oops!", message: "All fields must be filled!"), animated: true, completion: nil)
        }
        else if title.count < 5 {
            present(Helper.pushAlert(title: "Oops!", message: "Title must be more than 5 characters!"), animated: true, completion: nil)
        }
        else {
            let context = appDelegate.persistentContainer.viewContext
            
            let username = UserDefaults.standard.string(forKey: "USERNAME")
            do{
                let fetchRequest = User.checkUserExist(username: username!)
                let result = try context.fetch(fetchRequest)
                
                let diary = Diary(context: context)
                diary.title = title
                diary.content = content
                diary.date = Date()
                diary.user = result[0]
                
                context.insert(diary)
                try context.save()
                
            } catch {
                print(error.localizedDescription)
            }
            
            performSegue(withIdentifier: "fromAddToHomepageSegue", sender: self)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func titleDidEndOnExit(_ sender: Any) {
        
    }
    
}
