//
//  ProfileViewController.swift
//  Viary
//
//  Created by Andre Lay, Gregorius Albert, and Kevin Putra Yonathan on 03/12/21.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fullnameLabel.text = UserDefaults.standard.string(forKey: "FULLNAME")
        usernameLabel.text = UserDefaults.standard.string(forKey: "USERNAME")
    }
    
    @IBAction func logout(_ sender: Any) {
        
        let alert = UIAlertController(
            title: "Log out from Viary",
            message: "Are you sure to log out?",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: "Log Out",
            style: .destructive,
            handler: {
                (alert: UIAlertAction!) in
                self.performSegue(withIdentifier: "profileToLoginSegue", sender: self)
            }
        ))
        
        alert.addAction(UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: nil
        ))
        
        present(alert, animated: true, completion: nil)
    }
}
