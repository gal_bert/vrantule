//
//  EditDiaryViewController.swift
//  Viary
//
//  Created by Andre Lay, Gregorius Albert, and Kevin Putra Yonathan on 03/12/21.
//

import UIKit

class EditDiaryViewController: UIViewController {
    @IBOutlet weak var titleTextfield: UITextField!
    @IBOutlet weak var contentTextview: UITextView!
    
    var diary:Diary?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.initializeTextview(textView: contentTextview)
        titleTextfield.text = diary!.title
        contentTextview.text = diary!.content
    }
    
    @IBAction func update(_ sender: Any) {
        let title = titleTextfield.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let content = contentTextview.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if title == "" || content == "" {
            present(Helper.pushAlert(title: "Oops!", message: "All fields must be filled!"), animated: true, completion: nil)
        }
        else if title.count < 5 {
            present(Helper.pushAlert(title: "Oops!", message: "Title must be more than 5 characters!"), animated: true, completion: nil)
        }
        else {
            let context = appDelegate.persistentContainer.viewContext
            
            diary?.title = title
            diary?.content = content
            
            do {
                try context.save()
            }
            catch {
                print(error.localizedDescription)
            }
            
            performSegue(withIdentifier: "fromEditToHomepageSegue", sender: self)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func titleDidEndOnExit(_ sender: Any) {
    
    }
    
    
    
}
